#include"1dIsing.h"

/*
	Main device code for the 1D fractional step
*/



//=========================================================================

//Amazing helper function. Use it to get the correct errors back from the 
//CUDA functions.
#define checkCudaErrors(err)  __checkCudaErrors (err, __FILE__, __LINE__)

inline void __checkCudaErrors(cudaError err, const char *file, const int line )
{
    if(cudaSuccess != err)
    {
        fprintf(stderr, "%s(%i) : CUDA Runtime API error %d: %s.\n",file,
							line, (int)err, cudaGetErrorString( err ) );
        exit(-1);        
    }
}
//========================================================================







int main(void){

//N : Size of the system
int N = 2 * NUMBER_OF_PARTS * NBLOCKS * NTHREADS;

int *lat = (int*)malloc(N * sizeof(int));
int *cov; //= (int*)malloc(NBLOCKS*sizeof(int));

int *devLat, i, j, *devCov;
float meanCov;

//States for the random number generator
curandState *devStates;


clock_t start, end;

reactData react;


//cov = (int*)malloc(NBLOCKS*sizeof(int));

// Allocate pinned memory for better performance 

cudaMallocHost(&cov, NBLOCKS * sizeof(int));	

//==============================================

FILE *results = NULL;
	
	
	results = fopen("RESULTS.txt","w");
	if(results == NULL){
		printf("Couldn't open file for saving the results. Will exit\n");	
		exit(-1);
	}

//===============================================
	//Initialize the react structure
	react.h  = 0.0;
	react.b  = 2.0;
	react.J  = 1.0;
	react.cd = 1.0;
	react.ca = 1.0; 
//==============================================



float deltaT = 1.0; //(float)(FINAL_T + (STEPS_IN_TIME - 1.0))/(float)STEPS_IN_TIME;

//=====================================================

//Check if there is an actual device in the system

int cudaCount;
checkCudaErrors(cudaGetDeviceCount(&cudaCount));

if(cudaCount == 0){
	printf("Program can't run without a GPU. Will now exit.\n");
	exit(1);
}
else{
	printf("Found at least one device!\n\n");
}

//====================================================


// Allocate space for the lattice on the device
checkCudaErrors(cudaMalloc((void**)&devLat, N * sizeof(int)));

checkCudaErrors(cudaMalloc((void**)&devCov, NBLOCKS*sizeof(int)));

// Allocate space for the RNG states on the device
checkCudaErrors(cudaMalloc((void**)&devStates, NTHREADS * NBLOCKS * sizeof(curandState)));


//=====================================================

printf("========== CUDA fractional step - 1D =======\n");
printf(" Final time we want to reach : %d\n",FINAL_T);
printf(" Deltat : %1.2f\n",deltaT);
printf(" Blocks used  : %d\n",NBLOCKS);
printf(" Threads used : %d\n", NTHREADS);
printf(" Number of cells in each part : %d\n",NUMBER_OF_PARTS);
printf(" System size  : %d\n", N);


//========================================

//Initialize the lattice with values
for(i = 0; i < N; i++){
	lat[i] = 1;
} 


//=======================================


printf(" Lattice initialized. Locked & ready for computation!\n");

printf("==============================================\n\n");

// Copy the lattice onto the GPU
		checkCudaErrors(cudaMemcpy(devLat, lat, N * sizeof(int), cudaMemcpyHostToDevice));

// Initalize the random number generators for the threads	
		initRandomSequences<<<NBLOCKS,NTHREADS>>>(devStates,1403);
		
		
start = clock();	
printf("\n    h\t\tMean coverage\t\tExact solution\t\tError\n");


benchH(lat, devLat, N, deltaT, cov,  devCov, react, results, devStates);


end = clock();


printf("Timing : %f\n",(double)(end - start)/CLOCKS_PER_SEC);

//======================================
//Clean on the card side
printf(" Freeing memory on the card side . . . ");
cudaFree(devLat);
cudaFree(devCov);
printf("Success\n");
//=======================================

//Clean on the host side 
printf(" Freeing memory on the host side . . .");
free(lat);

// Free pinned memory
cudaFreeHost(cov);
printf("Success\n");
//=======================================

//Close the file
fclose(results);

printf("====== Program finished ==============\n\n");

return 0;
}



