#include"1dIsing.h"



double randU(void);
float computeRatesSSA(int*lat, int index, int N,  reactData react);


float ssa(int* lat, int N, int finalT, reactData react){

int i, j, k;
float* rates = NULL;
float u, t;
float rateSum = 0.0;
float tempSum;
int cov = 0;

	rates = (float*)malloc(N * sizeof(float));
	if(rates == NULL){
		printf("No memory for rates\n");
		exit(1);
		}	
		
	for(i = 0; i < N;i++){
		rates[i] = computeRatesSSA(lat, i, N, react);
		rateSum += rates[i];
	}	
	
	t = -log(randU())/rateSum;
	
	for(j =0; j < finalT; j++){
	//while(t < finalT){
		tempSum = 0.0;
		u = randU() * rateSum;
		
		for(i = 0;i < N; i++){
			tempSum += rates[i];
			if(tempSum > u){
				rateSum = 0.0;
				lat[i]  = 1 - lat[i];
				rates[i] = computeRatesSSA(lat, i, N, react);
				if(i == (N-1)){
				//	rateSum -= (rates[N - 2] + rates[0]);
					rates[N - 2] = computeRatesSSA(lat, N - 2 , N, react);
					rates[0]     = computeRatesSSA(lat, 0, N, react);
				//	rateSum += (rates[N - 2] + rates[0]);
				}
				else if(i == 0){
				//	rateSum -= (rates[1] + rates[N - 1]);
					rates[1]     = computeRatesSSA(lat, 1, N, react);
					rates[N - 1] = computeRatesSSA(lat, N - 1, N, react); 
				//	rateSum += (rates[1] + rates[N - 1]);
				}
				else{
				//	rateSum -= (rates[i - 1] + rates[i + 1]);
					rates[i - 1] = computeRatesSSA(lat, i - 1, N, react);
					rates[i + 1] = computeRatesSSA(lat, i + 1, N, react);
				//	rateSum += (rates[i - 1] + rates[i + 1]);
				}
				for(k = 0;k < N;k++){
					rateSum += rates[k];
				}
				break;
			}
		}
		t += -log(randU())/rateSum;
		for(i = 0;i < N;i++)
			cov += lat[i];
	}


	
		
	free(rates);	
	return (float)cov/(N * finalT * 1.0);

}



//===================================================================

float computeRatesSSA(int*Lat, int index, int N,  reactData react){
int prIndex, neIndex;

//==================================
// Chemical reaction specific constants
float h  = react.h;
float b  = react.b;
float J  = react.J;
float cd = react.cd;
float ca = react.ca;
//==================================

float U,r;

	if(index == 0 ){
		prIndex = (N - 1);
		neIndex = 1;
	}
	else if(index == (N - 1)){
		prIndex = (N - 2);
		neIndex = 0;
	}
	else{
		prIndex = (index - 1);
		neIndex = (index + 1);
	}


U = J * (Lat[prIndex] + Lat[neIndex]) - h;

r = cd *exp(-b * U) * Lat[index] + ca * (1 - Lat[index]) ;

return  r;
}

//====================================================================

double randU(void){

	return (double)rand()/(RAND_MAX+1.0);

}



