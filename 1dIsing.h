#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<curand.h>
#include<curand_kernel.h>


// This parameter doesn't change anything in the program at the moment. 
#define FINAL_T 1000

// Number of threads to use
#define NTHREADS 10

// Number of blocks to use
#define NBLOCKS  5

// How many steps should the program do. 
#define STEPS_IN_TIME 1000

//Number of elements on which each thread will do computations
#define NUMBER_OF_PARTS 10

// How many points to be used for the external field. Used for the mean coverage benchmark. 
#define STEPS_IN_H 20

// How many samples to drop in each computation. Should be smaller than STEPS_IN_TIME. Used for the mean coverage benchmark. 
#define BURNING 30



//Struct that holds all the data for a particular reaction
typedef struct
{
	float h;
	float b;
	float J;
	float cd;
	float ca;

} reactData;


//============ Host functions ====================================================
void benchH(int* lat, int* devLat, int N,float deltaT, 
		int *cov, int*devCov, reactData react, FILE* results,curandState *devStates);
float fractionalStep(int*Lat, int* devLat,int steps, 
reactData react, curandState *devStates, float deltaT, int N, int* cov, int*devCov);
float exactMagnetization(reactData react);
float ssa(int* lat, int N, int finalT, reactData react);

//=========== Device functions ===================================================
__global__ void L(int* Lat, int N, float Dt, int Number_Of_Partitions, int NThreads,int var, reactData react,curandState *globalState, int*devCov);
__device__ float computeRates(int* Lat, int index, int N, reactData react);
__global__ void initRandomSequences(curandState *state, int seed);

