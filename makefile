#Gourgoulias Konstantinos
#kgourgou@cns.umass.edu

CC = nvcc
CFLAGS = -arch=sm_20
OBJECTS = main.o lOdd.o fractionalStep.o bench.o ssa.o

a.out : $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -lm

%.o      : %.cu 1dIsing.h
	$(CC) $(CFLAGS) -c $<

cleanrun : 
	  nvcc $(CFLAGS) main.cu lOdd.cu fractionalStep.cu 


clean    : 
		rm *.o



