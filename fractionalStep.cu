#include"1dIsing.h"


//Fractional step algorithm


//N = size of the lattice
float fractionalStep(int* lat, int* devLat,int steps, reactData react, curandState *devStates, float deltaT, int N, int *cov, int*devCov){
int i,l;
float meanCov = 0.0;
int newCoverage = 0; 


for(i = 0; i < N; i++){
	meanCov += lat[i];
}

for(i = 0; i < steps; i++){

		L<<<NBLOCKS, NTHREADS>>>(devLat, N, deltaT, NUMBER_OF_PARTS, NTHREADS,0,react,devStates, devCov);
		
	
		
		L<<<NBLOCKS, NTHREADS>>>(devLat, N, deltaT, NUMBER_OF_PARTS, NTHREADS,1,react,devStates,devCov);

	//===============================================
	
		//Calculate the mean coverage of the last (STEPS_IN_TIME - BURNING) samples
		
		  cudaMemcpy(lat, devLat, N * sizeof(int), cudaMemcpyDeviceToHost);
		newCoverage  = 0; 
	
		for(l = 0;l < N; l++)
			newCoverage += lat[l];
		meanCov += newCoverage;
			
	}

       meanCov = meanCov/(N*(steps)*1.0);

return meanCov;
	
}
