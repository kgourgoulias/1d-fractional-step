#include"1dIsing.h"


/* Benchmarks file */


void benchH(int* lat, int* devLat, int N,float deltaT, 
		int *cov, int*devCov, reactData react, FILE* results, curandState *devStates){

	int j = 0;
	float meanCov;
	float ssaCov;
	int *ssaLat = (int*)malloc(N * sizeof(int));
		if(ssaLat == NULL){
			printf("ssaLat = NULL\n");
			exit(1);
		}
	for(j = 0;j < N;j++)
		ssaLat[j] = lat[j];
	
	for(j = 0; j < STEPS_IN_H; j++){

		react.h = j*2.0/(STEPS_IN_H - 1);
	
		//Burning time
		 fractionalStep(lat,devLat,BURNING,
						react, devStates, deltaT, N, cov, devCov);
		 
		//Actual computation
		 meanCov = fractionalStep(lat,devLat,(STEPS_IN_TIME - BURNING),
							react, devStates, deltaT, N, cov, devCov);
		
		 ssaCov = ssa(ssaLat, N, STEPS_IN_TIME*2, react); // Fix the whole realization thing

		fprintf(results,"%f\t%f\n",react.h,meanCov);
		printf("%f\t%f\t%f\t%f\t%1.2e\n",react.h,meanCov,ssaCov,
			exactMagnetization(react),fabs(meanCov-exactMagnetization(react)));
	}

}



//============================================================

//% exact solution of the mean coverage of the Ising model
//% with spins in {0,1}
float exactMagnetization(reactData react){
float b = react.b;
float h = react.h;
float K01 = react.J;
h = -h/2+K01/2;
float K = K01/4;

float a = sinh(b*h);
      b = a*a + exp(-b*4*K);
float y = a/sqrt(b);
y = (y+1)/2;
return y;
}



