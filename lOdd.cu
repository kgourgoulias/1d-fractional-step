#include"1dIsing.h"


//N here is the system size constant


//var = 0 : odd
//var = 1 : even

__global__ void L(int* Lat, int N, float Dt, int NumOfPar, int Nthreads, int var,reactData react ,curandState *globalState, int* devCov){

int localIndex = NumOfPar * (2 * threadIdx.x + var) + NumOfPar * 2 * blockDim.x * blockIdx.x;

int idofX = threadIdx.x + blockIdx.x * blockDim.x;
int totalCov = 0;
int offSet = NumOfPar, i;
float t = 0.0, rateSum, u, temp;

curandState s  = globalState[idofX];

//Sum of rates
		for(i = localIndex; i < (localIndex + offSet);i++){
				rateSum += computeRates(Lat, i, N, react);
			}
	
		t = -log(curand_uniform(&s))/rateSum;
	
	while(t < Dt){



		
		//Do KMC on that part
		temp = 0.0;
		u =  curand_uniform(&s);
		u = rateSum * u;
		
		for(i = localIndex; i < (localIndex + offSet); i++){

			temp  += computeRates(Lat,i,N,react);
			
			if(temp > u){
			
				Lat[i]  = 1 - Lat[i];
				break;
				
			}
		}
		//Sample from the exponential distribution and add to the total time
		rateSum = 0.0;
		for(i = localIndex; i < (localIndex + offSet);i++){
				rateSum += computeRates(Lat, i, N, react);
			}
			
		t += -log(curand_uniform(&s))/rateSum;
	}

	
 	/*__syncthreads();
 	//Calculate the coverage
	if(threadIdx.x == 0){
		
		int end = NumOfPar * (2 * (NTHREADS - 1) + var) +
			 NumOfPar * 2 * blockDim.x * blockIdx.x;
			 
		for(i = localIndex; i < (end + offSet);i++){		
                	totalCov += Lat[i];
		}
		
		devCov[blockIdx.x]  = totalCov;
	}
	*/

        globalState[idofX] = s;
}


// Computes the rates. This version works only for nearest neighbor
// interactions.

__device__ float computeRates(int* Lat, int index, int N, reactData react){
int prIndex, neIndex;

//==================================
// Chemical reaction specific constants
float h  = react.h;
float b  = react.b;
float J  = react.J;
float cd = react.cd;
float ca = react.ca;
//==================================

float U,r;

	if(index == 0 ){
		prIndex = (N - 1);
		neIndex = 1;
	}
	else if(index == (N - 1)){
		prIndex = (N - 2);
		neIndex = 0;
	}
	else{
		prIndex = (index - 1);
		neIndex = (index + 1);
	}


U = J * (Lat[prIndex] + Lat[neIndex]) - h;

r = cd *exp(-b * U) * Lat[index] + ca * (1 - Lat[index]) ;

return  r;
}




// Setup the random number generators
__global__ void initRandomSequences(curandState *state, int seed)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;

		curand_init(seed + id, 0 , 0, &state[id]) ;

	}



